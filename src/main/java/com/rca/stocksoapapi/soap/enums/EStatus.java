package com.rca.stocksoapapi.soap.enums;

public enum EStatus {
    NEW, GOOD_SHAPE, OLD
}
