package com.rca.stocksoapapi.soap.repository;

import com.rca.stocksoapapi.soap.bean.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IItemRepository extends JpaRepository<Item, Long> {
}
