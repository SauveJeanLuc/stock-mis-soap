package com.rca.stocksoapapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockSoapApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockSoapApiApplication.class, args);
    }

}
